{ pkgs ? import <nixpkgs> }:
pkgs.mkShell let
  getPatches = dir:
    let files = builtins.attrNames (builtins.readDir dir);
    in map (f: dir + ("/" + f)) files;
  version = "3.0.5";
  channel = "stable";
  filename = "flutter_linux_${version}-${channel}.tar.xz";

  # Decouples flutter derivation from dart derivation,
  # use specific dart version to not need to bump dart derivation when bumping flutter.
  dartVersion = "2.17.5";
  dartSourceBase = "https://storage.googleapis.com/dart-archive/channels";
  dartForFlutter = pkgs.dart.override {
    version = dartVersion;
    sources = {
      "${dartVersion}-x86_64-linux" = pkgs.fetchurl {
        url = "${dartSourceBase}/stable/release/${dartVersion}/sdk/dartsdk-linux-x64-release.zip";
        sha256 = "sha256-AFJGeiPsjUZSO+DykmOIFETg2jIohg62tp3ghZrKJFk=";
      };
    };
  };
in
{
  buildInputs = [
    pkgs.flutterPackages.mkFlutter
    {
      inherit version;
      dart = dartForFlutter;
      pname = "flutter";
      src = pkgs.fetchurl {
        url = "https://storage.googleapis.com/flutter_infra_release/releases/${channel}/linux/${filename}";
        sha256 = "sha256-vh3QjLGFBN321DUET9XhYqSkILjEj+ZqAALu/mxY+go=";
      };
      #patches = getPatches ./patches;

    }
  ];
}
