# Patches:

git-dir.patch is modified from nixpkgs @ 02e10382d53894fdac31b6ac02c6eca3194cbdc0

# Things to modify when making new avd

##### For making new avd `avdmanager create avd --name android32 --package "system-images;android-32;google_apis_playstore;x86_64"`

##### Config file at ~/.android/avd/avd-instance-name.avd/config.ini

- hw.lcd.width -> 640 works well for a phone form factor with 240dpi
- hw.lcd.height -> 1280 works well for a phone form factor with 240dpi
- hw.lcd.density -> one value of 120, 160, 240, 213, 320
- hw.keyboard -> yes
- hw.audioInput -> no - based on needs, reduces idle CPU usage
- hw.audioOutput -> no - based on needs
