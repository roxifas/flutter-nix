{ pkgs ? import <nixpkgs> { }
}:

(pkgs.buildFHSEnv {
  name = "flutter-env";
  multiPkgs = pkgs: [
    # Flutter only use these certificates
    (pkgs.runCommand "fedoracert" { } ''
      mkdir -p $out/etc/pki/tls/
      ln -s ${pkgs.cacert}/etc/ssl/certs $out/etc/pki/tls/certs
    '')
    pkgs.zlib
  ];
  targetPkgs = pkgs:
    with pkgs; with pkgs.xorg; [
      bash
      curl
      dart
      git
      unzip
      which
      xz

      # flutter test requires this lib
      libGLU

      # for android emulator
      alsa-lib
      dbus
      expat
      libpulseaudio
      libuuid
      libX11
      libxcb
      libXcomposite
      libXcursor
      libXdamage
      libXext
      libXfixes
      libXi
      libXrender
      libXtst
      libGL
      nspr
      nss
      systemd

      # Additional
      vscodium
      # Adjust jdk version as needed, old gradle WILL break with jdk18
      # https://docs.gradle.org/current/userguide/compatibility.html
      jdk17
      # Used in testing one of my applications
      sqlite
    ];
  profile = ''
    export PATH="$PATH":"$HOME/.pub-cache/bin:$HOME/dev/flutter/bin"
    export ANDROID_HOME="$HOME/dev/android-sdk"
  '';
  runScript = "bash";
}).env
