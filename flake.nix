{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    android = {
      url = "github:tadfisher/android-nixpkgs/stable";
      #inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, android }:
    let
      pkgs = import nixpkgs {
        config = { allowUnfree = true; };
        system = "x86_64-linux";
      };
    in
    rec {
      packages.x86_64-linux = {
        android-sdk = android.sdk.x86_64-linux (sdkPkgs: with sdkPkgs; [
          build-tools-30-0-3
          # Holy shit, the ordering of tools and cmdline-tools-latest is SUPER important, otherwise avdmanager shits itself
          # tools
          cmdline-tools-latest
          emulator
          platform-tools
          platforms-android-28
          platforms-android-29
          platforms-android-30
          platforms-android-31
          platforms-android-32
          platforms-android-33
          platforms-android-34
          system-images-android-30-google-apis-x86
          system-images-android-32-google-apis-playstore-x86-64
          # patcher-v4
          # No idea why but flutter suddenly started wanting this installed, at flutter v3.10.0
          ndk-25-1-8937393
          cmake-3-22-1
        ]);
      };
      devShells.x86_64-linux.default = devShells.x86_64-linux.fvm-fhs;
      devShells.x86_64-linux.testing = pkgs.mkShell {
        buildInputs =
          [
            packages.x86_64-linux.android-sdk
            pkgs.jdk17
            pkgs.dart
          ];
        ANDROID_HOME = packages.x86_64-linux.android-sdk;
        # PATH = "$PATH:$HOME/.pub-cache/bin";
      };
      devShells.x86_64-linux.fvm-fhs = (pkgs.buildFHSUserEnv {
        name = "fvm-fhs";
        multiPkgs = pkgs: [
          # Flutter only use these certificates
          (pkgs.runCommand "fedoracert" { } ''
            mkdir -p $out/etc/pki/tls/
            ln -s ${pkgs.cacert}/etc/ssl/certs $out/etc/pki/tls/certs
          '')
          pkgs.zlib
        ];
        targetPkgs = pkgs:
          with pkgs; [
            bash
            curl
            dart
            git
            unzip
            which
            xz

            # flutter test requires this lib
            libGLU

            # For android emulator (Using our own nix-defined android-sdk does not need these)
            # alsa-lib
            # dbus
            # expat
            # libpulseaudio
            # libuuid
            # libX11
            # libxcb
            # libXcomposite
            # libXcursor
            # libXdamage
            # libXext
            # libXfixes
            # libXi
            # libXrender
            # libXtst
            # libGL
            # nspr
            # nss
            # systemd

            # Additional
            packages.x86_64-linux.android-sdk
            # (pkgs.writeShellScriptBin "flutter" ''fvm flutter "$@"'')
            # vscodium
            # Adjust jdk version as needed, old gradle WILL break with jdk18
            # https://docs.gradle.org/current/userguide/compatibility.html
            jdk17
            # Used in testing one of my applications
            sqlite
            # For building linux applications
            cmake
            ninja
            clang
            pkg-config
          ];
        profile = ''
          export PATH="$PATH":"$HOME/.pub-cache/bin"
          export ANDROID_HOME=${packages.x86_64-linux.android-sdk}/share/android-sdk/;
          export CHROME_EXECUTABLE="chromium"
          unset LD_LIBRARY_PATH
        '';
        runScript = "bash";
      }).env;
    };
}
